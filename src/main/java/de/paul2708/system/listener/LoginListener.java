package de.paul2708.system.listener;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.util.Util;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class LoginListener implements Listener {

    @EventHandler
    public void onLogin(LoginEvent e) {
        UUID uuid = e.getConnection().getUniqueId();
        String name = e.getConnection().getName();

        // Check if player is banned
        Abuse abuse = BungeeSystem.getInstance().getAbuseManager().isBanned(uuid);
        if (abuse != null) {
            String kickReason = "§b§lBlitzCookie\n" +
                    "§7Du wurdest vom Netzwerk gebannt\n\n" +
                    "§bGrund: §4" + abuse.getReason() + "\n" +
                    "§aZeit: §c" + (abuse.getLastTo() == -1L ? "PERMANENT" : Util.getTime(abuse.getLastTo()));
            e.setCancelReason(kickReason);
            e.setCancelled(true);
            return;
        }

        BungeeSystem.getInstance().getAbuseManager().cache(uuid, name);
    }
}
