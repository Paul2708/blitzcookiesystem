package de.paul2708.system.listener;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.util.Constants;
import de.paul2708.system.util.Util;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

/**
 * Created by Paul on 29.07.2017.
 */
public class ChatListener implements Listener {

    @EventHandler
    public void onChat(ChatEvent e) {
        if (e.getSender() instanceof ProxiedPlayer) {
            if (e.getMessage().startsWith("/")) {
                return;
            }

            ProxiedPlayer player = (ProxiedPlayer) e.getSender();
            Abuse abuse = BungeeSystem.getInstance().getAbuseManager().isMuted(player.getUniqueId());

            if (abuse != null) {
                player.sendMessage(new TextComponent(Constants.TAG + "§7Du bist gemutet."));
                player.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + abuse.getReason()));
                player.sendMessage(new TextComponent(Constants.TAG + "§aEnde: §e" + Util.getTime(abuse.getLastTo())));

                e.setCancelled(true);
            }
        }
    }
}
