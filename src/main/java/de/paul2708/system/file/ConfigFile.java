package de.paul2708.system.file;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.IOException;

/**
 * Created by Paul on 28.07.2017.
 */
public class ConfigFile {

    private File directory;
    private File configFile;
    private Configuration configuration;

    private boolean first;

    public ConfigFile(File directory) {
        this.directory = directory;

        this.first = false;
    }

    public void load() {
        try {
            // Create directory
            if (!directory.exists()) {
                directory.mkdir();
            }

            // Create file
            this.configFile = new File(directory.getPath(), "config.yml");
            if (!configFile.exists()) {
                configFile.createNewFile();
                this.first = true;

                BungeeSystem.getInstance().log(Constants.TAG + "§cDie config.yml wurde erstellt. Bitte starte den Server neu.");
            }

            // Load configuration
            this.configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);

            // Create default value
            if (first) {
                createDefaultValues();

                ProxyServer.getInstance().stop(Constants.TAG + "§cSystem wird konfiguriert..");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void createDefaultValues() {
        configuration.set("mysql.ip", "localhost");
        configuration.set("mysql.database", "bungeesystem");
        configuration.set("mysql.user", "root");
        configuration.set("mysql.password", "");

        try {
            ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getIp() {
        return configuration.getString("mysql.ip");
    }

    public String getDatabase() {
        return configuration.getString("mysql.database");
    }

    public String getUser() {
        return configuration.getString("mysql.user");
    }

    public String getPassword() {
        return configuration.getString("mysql.password");
    }
}
