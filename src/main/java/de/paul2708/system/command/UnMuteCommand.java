package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class UnMuteCommand extends Command {

    // TODO: Remove permission comment

    public UnMuteCommand() {
        super("unmute");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/unmute <Spieler>"));
        } else {
            String name = args[0];

            // Check permission
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (!player.hasPermission(Constants.UNMUTE_PERMISSION)) {
                    player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                    return;
                }
            }

            // Check if player exists
            Object[] data = BungeeSystem.getInstance().getAbuseManager().getPlayerData(name);

            if (data.length == 0) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Name " + name + " ist nicht bekannt."));
                return;
            }

            // Check if player is banned
            UUID uuid = (UUID) data[0];

            Abuse abuse = BungeeSystem.getInstance().getAbuseManager().isMuted(uuid);
            if (abuse == null) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler " + name + " ist nicht gemutet."));
                return;
            }

            // Unmute player
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(uuid);
            if (target != null && target.isConnected()) {
                target.sendMessage(new TextComponent(Constants.TAG + "§7Du darfst nun wieder schreiben."));
            }

            sender.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + data[1] + " §7entmutet."));

            BungeeSystem.getInstance().getAbuseManager().removeAbuse(abuse);
        }
    }
}
