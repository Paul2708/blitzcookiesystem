package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.abuse.AbuseAction;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class BanCommand extends Command {

    public BanCommand() {
        super("ban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/ban <Spieler> [Grund]"));
        } else {
            String name = args[0];
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

            // Check permission
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (!player.hasPermission(Constants.BAN_PERMISSION)) {
                    player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                    return;
                }
            }
            // Check if target is himself
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (target.getUniqueId().equals(player.getUniqueId())) {
                    player.sendMessage(new TextComponent("§cDu kannst dich selbst nicht bannen."));
                    return;
                }
            }
            // Check if target is team member
            if (target != null && target.isConnected() && target.hasPermission(Constants.BYPASS_PERMISSION)) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDu darfst den Spieler §e" + target.getName() + " §cnicht bannen."));
                return;
            }

            // Build reason
            String reason = "";
            if (args.length == 1) {
                reason = "-/-";
            } else {
                for (int i = 1; i < args.length; i++) {
                    reason += args[i] + " ";
                }
            }

            // Get target data
            Object[] data = BungeeSystem.getInstance().getAbuseManager().getPlayerData(name);

            if (data.length == 0) {
                sender.sendMessage(Constants.TAG + "§cDie UUID des Spieler konnte nicht gefunden werden.");
                return;
            }

            // Check if target is banned
            if (BungeeSystem.getInstance().getAbuseManager().isBanned((UUID) data[0]) != null) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler ist bereits gebannt."));
                return;
            }

            Abuse abuse = new Abuse()
                    .action(AbuseAction.BAN)
                    .staff(sender instanceof ProxiedPlayer ? sender.getName() : "Console")
                    .abuser((String) data[1])
                    .reason(reason)
                    .timeStamp(System.currentTimeMillis())
                    .lastTo(-1L)
                    .staffUuid(sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getUniqueId() : UUID.randomUUID())
                    .abuserUuid((UUID) data[0]);

            BungeeSystem.getInstance().getAbuseManager().addAbuse(abuse);

            if (target != null && target.isConnected()) {
                String kickReason = "§b§lBlitzCookie\n§7Du wurdest vom Netzwerk gebannt\n\n§bGrund: §4" + reason + "\n§aEnde: §cPERMANENT";
                target.disconnect(new TextComponent(kickReason));
            }

            sender.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + data[1] + " §7vom Netzwerk gebannt."));
            sender.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + reason));
        }
    }
}
