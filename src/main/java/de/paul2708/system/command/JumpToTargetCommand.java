package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 07.08.2016.
 */
public class JumpToTargetCommand extends Command {

    public JumpToTargetCommand() {
        super("jumptotarget");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            BungeeSystem.getInstance().log("Nur für Spieler");
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        if (args.length == 1) {
            if (player.hasPermission(Constants.GET_REPORTS_PERMISSION)) {
                ServerInfo info = ProxyServer.getInstance().getServerInfo(args[0]);
                if (info != null) {
                    player.connect(ProxyServer.getInstance().getServerInfo(args[0]));
                }
            }
        }
    }
}
