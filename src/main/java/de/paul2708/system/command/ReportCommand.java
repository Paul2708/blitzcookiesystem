package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 07.08.2016.
 */
public class ReportCommand extends Command {

    public ReportCommand() {
        super("report");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        if (!(commandSender instanceof ProxiedPlayer)) {
            BungeeSystem.getInstance().log(Constants.TAG + "§cDer Report-Befehl ist nur für Spieler.");
            return;
        }

        ProxiedPlayer player = (ProxiedPlayer) commandSender;

        if (args.length == 0 || args.length == 1) {
            player.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/report <Spieler> <Grund>"));
        } else {
            String name = args[0];
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

            // Check if player is online
            if (target == null) {
                player.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler §e" + name + " §cist offline."));
                return;
            }
            // Check if target is himself
            if (target.getUniqueId().equals(player.getUniqueId())) {
                player.sendMessage(new TextComponent(Constants.TAG + "§cDu kannst dich selbst nicht reporten."));
                return;
            }
            // Check if target is team member
            if (target.hasPermission(Constants.BYPASS_PERMISSION)) {
                player.sendMessage(new TextComponent(Constants.TAG + "§cDu darfst den Spieler §e" + target.getName() + " §cnicht reporten."));
                return;
            }

            // Build reason
            String reason = "";
            for (int i = 1; i < args.length; i++) {
                reason += args[i] + " ";
            }

            // Report target
            player.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + target.getName() + " §7erfolgreich reportet."));

            TextComponent clickMessage = new TextComponent(Constants.TAG + "§7Zum teleportieren klicken.");
            clickMessage.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT,
                    new ComponentBuilder("§eSpringe auf " + target.getServer().getInfo().getName()).create()));
            clickMessage.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/jumptotarget " + target.getServer().getInfo().getName()));

            for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                if (all.hasPermission(Constants.GET_REPORTS_PERMISSION)) {
                    all.sendMessage(new TextComponent(Constants.TAG + "§a" + player.getName() + " §7hat §c" + target.getName() + " §7reportet."));
                    all.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + reason));
                    all.sendMessage(clickMessage);
                }
            }
        }
    }
}
