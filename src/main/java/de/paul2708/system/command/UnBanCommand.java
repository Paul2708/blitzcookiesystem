package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;

/**
 * Created by Paul on 07.08.2016.
 */
public class UnBanCommand extends Command {

    public UnBanCommand() {
        super("unban");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/unban <Spieler>"));
        } else {
            String name = args[0];

            // Check permission
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (!player.hasPermission(Constants.BAN_PERMISSION)) {
                    player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                    return;
                }
            }

            // Check if player exists
            Object[] data = BungeeSystem.getInstance().getAbuseManager().getPlayerData(name);

            if (data.length == 0) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Name " + name + " ist nicht bekannt."));
                return;
            }

            // Check if player is banned
            UUID uuid = (UUID) data[0];

            Abuse abuse = BungeeSystem.getInstance().getAbuseManager().isBanned(uuid);
            if (abuse == null) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler " + name + " ist nicht gebannt."));
                return;
            }

            // Unban player
            sender.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + data[1] + " §7entbannt."));

            BungeeSystem.getInstance().getAbuseManager().removeAbuse(abuse);
        }
    }
}
