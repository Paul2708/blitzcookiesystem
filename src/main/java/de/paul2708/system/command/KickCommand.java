package de.paul2708.system.command;

import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 07.08.2016.
 */
public class KickCommand extends Command {

    public KickCommand() {
        super("kick");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/kick <Spieler> [Grund]"));
        } else {
            String name = args[0];
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

            // Check permission
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (!player.hasPermission(Constants.KICK_PERMISSION)) {
                    player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                    return;
                }
            }
            // Check if player is online
            if (target == null) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler §e" + name + " §cist offline."));
                return;
            }
            // Check if target is himself
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (target.getUniqueId().equals(player.getUniqueId())) {
                    player.sendMessage(new TextComponent("§cDu kannst dich selbst nicht kicken."));
                    return;
                }
            }
            // Check if target is team member
            if (target.hasPermission(Constants.BYPASS_PERMISSION)) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDu darfst den Spieler §e" + target.getName() + " §cnicht kicken."));
                return;
            }

            // Build reason
            String reason = "";
            if (args.length == 1) {
                reason = "-/-";
            } else {
                for (int i = 1; i < args.length; i++) {
                    reason += args[i] + " ";
                }
            }

            // Kick target
            String kickReason = "§b§lBlitzCookie\n§cDu wurdest vom Netzwerk geworfen\n\n§bGrund: §4" + reason;
            target.disconnect(new TextComponent(kickReason));

            sender.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + target.getName() + " §7vom Netzwerk geworfen."));
            sender.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + reason));
        }
    }
}
