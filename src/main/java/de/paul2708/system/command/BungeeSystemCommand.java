package de.paul2708.system.command;

import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Created by Paul on 07.08.2016.
 */
public class BungeeSystemCommand extends Command {

    public BungeeSystemCommand() {
        super("bungeesystem");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        // Check permission
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            if (!player.hasPermission(Constants.BUNGEESYSTEM_PERMISSION)) {
                player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                return;
            }
        }

        // Send messages
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/ban <Spieler> [Grund] §8| §7§obanne einen Spieler permanent."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/tempban <Spieler> <Dauer> <m|h|d|w> [Grund] §8| §7§obanne einen Spieler auf Zeit."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/unban <Spieler> §8| §7§oentbanne einen Spieler."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/kick <Spieler> [Grund] §8| §7§okicke einen Spieler."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/report <Spieler> [Grund] §8| §7§oreporte einen Spieler."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/mute <Spieler> <Dauer> <m|h|d|w> [Grund] §8| §7§omute einen Spieler auf Zeit."));
        sender.sendMessage(new TextComponent(Constants.TAG + "§e/unmute <Spieler> §8| §7§oentmute einen Spieler."));
    }
}
