package de.paul2708.system.command;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.abuse.AbuseAction;
import de.paul2708.system.util.Constants;
import de.paul2708.system.util.Util;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Created by Paul on 07.08.2016.
 */
public class MuteCommand extends Command {

    public MuteCommand() {
        super("mute");
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length <= 2) {
            sender.sendMessage(new TextComponent(Constants.TAG + "§cBenutze den Befehl §e/mute <Spieler> <Dauer> <m|h|d|w> [Grund]"));
        } else {
            String name = args[0];
            ProxiedPlayer target = ProxyServer.getInstance().getPlayer(name);

            // Check permission
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (!player.hasPermission(Constants.MUTE_PERMISSION)) {
                    player.sendMessage(new TextComponent("§cDu hast keine Rechte für diesen Befehl."));
                    return;
                }
            }
            // Check if target is himself
            if (sender instanceof ProxiedPlayer) {
                ProxiedPlayer player = (ProxiedPlayer) sender;
                if (target.getUniqueId().equals(player.getUniqueId())) {
                    player.sendMessage(new TextComponent("§cDu kannst dich selbst nicht muten."));
                    return;
                }
            }
            // Check if target is team member
            if (target != null && target.isConnected() && target.hasPermission(Constants.BYPASS_PERMISSION)) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDu darfst den Spieler §e" + target.getName() + " §cnicht muten."));
                return;
            }

            // Check duration
            long duration;
            try {
                duration = Integer.valueOf(args[1]);
            } catch (NumberFormatException e) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDie Angabe '" + args[1] + "' ist nicht korrekt."));
                return;
            }
            if (duration <= 0) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDie Angabe '" + args[1] + "' ist nicht korrekt."));
                return;
            }

            switch (args[2]) {
                case "m": case "M":
                    duration = TimeUnit.MINUTES.toMillis(duration);
                    break;
                case "h": case "H":
                    duration = TimeUnit.HOURS.toMillis(duration);
                    break;
                case "d": case "D":
                    duration = TimeUnit.DAYS.toMillis(duration);
                    break;
                case "w": case "W":
                    duration = TimeUnit.DAYS.toMillis(duration) * 7;
                    break;
                default:
                    sender.sendMessage(new TextComponent("§cDie Angabe '" + args[2] + "' ist nicht korrekt."));
                    return;
            }

            duration += System.currentTimeMillis();

            // Build reason
            String reason = "";
            if (args.length == 3) {
                reason = "-/-";
            } else {
                for (int i = 3; i < args.length; i++) {
                    reason += args[i] + " ";
                }
            }

            // Get target data
            Object[] data = BungeeSystem.getInstance().getAbuseManager().getPlayerData(name);

            if (data.length == 0) {
                sender.sendMessage(Constants.TAG + "§cDie UUID des Spieler konnte nicht gefunden werden.");
                return;
            }

            // Check if target is muted
            if (BungeeSystem.getInstance().getAbuseManager().isMuted((UUID) data[0]) != null) {
                sender.sendMessage(new TextComponent(Constants.TAG + "§cDer Spieler ist bereits gemutet."));
                return;
            }

            Abuse abuse = new Abuse()
                    .action(AbuseAction.MUTE)
                    .staff(sender instanceof ProxiedPlayer ? sender.getName() : "Console")
                    .abuser((String) data[1])
                    .reason(reason)
                    .timeStamp(System.currentTimeMillis())
                    .lastTo(duration)
                    .staffUuid(sender instanceof ProxiedPlayer ? ((ProxiedPlayer) sender).getUniqueId() : UUID.randomUUID())
                    .abuserUuid((UUID) data[0]);

            BungeeSystem.getInstance().getAbuseManager().addAbuse(abuse);

            if (target != null && target.isConnected()) {
                target.sendMessage(new TextComponent(Constants.TAG + "§7Du wurdest gemutet."));
                target.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + reason));
                target.sendMessage(new TextComponent(Constants.TAG + "§aEnde: §e" + Util.getTime(duration)));
            }


            sender.sendMessage(new TextComponent(Constants.TAG + "§7Du hast §c" + data[1] + " §7gemutet."));
            sender.sendMessage(new TextComponent(Constants.TAG + "§bGrund: §4" + reason));
            sender.sendMessage(new TextComponent(Constants.TAG + "§aEnde: §e" + Util.getTime(duration)));
        }
    }
}
