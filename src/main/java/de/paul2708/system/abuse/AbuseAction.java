package de.paul2708.system.abuse;

import java.io.Serializable;

/**
 * Created by Paul on 11.09.2016.
 */
public enum AbuseAction implements Serializable {

    MUTE("mute"),
    BAN("ban"),
    ;

    private String data;

    AbuseAction(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public static AbuseAction getActionByName(String action) {
        for (AbuseAction all : AbuseAction.values()) {
            if (all.getData().equalsIgnoreCase(action)) {
                return all;
            }
        }

        return null;
    }
}
