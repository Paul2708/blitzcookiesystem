package de.paul2708.system.abuse;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.mysql.DatabaseManager;
import de.paul2708.system.util.Constants;
import de.paul2708.system.util.Util;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import org.json.simple.JsonObject;
import org.json.simple.Jsoner;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by Paul on 28.07.2017.
 */
public class AbuseManager {

    private Map<UUID, String> playerCache;
    private List<Abuse> abuses;

    public AbuseManager() {
        this.playerCache = new ConcurrentHashMap<>();
        this.abuses = new CopyOnWriteArrayList<>();
    }

    public void resolveAbuses() {
        // Resolve all mutes and bans
        ResultSet rs = BungeeSystem.getInstance().getMySQL().getQuery("SELECT * FROM `abuses`");

        try {
            while (rs.next()) {
                String action = rs.getString("action");
                String staff = rs.getString("staff");
                String abuser = rs.getString("abuser");
                String reason = rs.getString("reason");
                String timeStamp = rs.getString("timestamp");
                String lastTo = rs.getString("last_to");
                String staffUuid = rs.getString("staff_uuid");
                String abuserUuid = rs.getString("abuser_uuid");

                Abuse abuse = new Abuse()
                        .action(AbuseAction.getActionByName(action))
                        .staff(staff)
                        .abuser(abuser)
                        .reason(reason)
                        .timeStamp(Long.valueOf(timeStamp))
                        .lastTo(Long.valueOf(lastTo))
                        .staffUuid(UUID.fromString(staffUuid))
                        .abuserUuid(UUID.fromString(abuserUuid));
                abuses.add(abuse);
            }

            // Clear old and inactive bans and mutes from list
            clear();
            BungeeSystem.getInstance().log(Constants.TAG + "§7Aktuell wurden §e" + abuses.size() + " §7Abuses geladen.");
        } catch (Exception e) {
            BungeeSystem.getInstance().log(Constants.TAG + "§cFehler beim Laden der Abuses");
            e.printStackTrace();
        }
    }

    public void addAbuse(Abuse abuse) {
        abuses.add(abuse);

        DatabaseManager.addAbuse(abuse);

        String log = Constants.TAG;
        if (abuse.getAction() == AbuseAction.BAN) {
            log = "§e" + abuse.getAbuser() + " §7(" + abuse.getAbuserUuid().toString() + ") " +
                    "§7wurde gebannt. (§eEnde: " + (abuse.getLastTo() == -1 ? "permanent" : Util.getTime(abuse.getLastTo())) + "§7)";
        } else if (abuse.getAction() == AbuseAction.MUTE) {
            log = "§e" + abuse.getAbuser() + " §7(" + abuse.getAbuserUuid().toString() + ") " +
                    "§7wurde gemutet. (§eEnde: " + (abuse.getLastTo() == -1 ? "permanent" : Util.getTime(abuse.getLastTo())) + "§7)";
        }

        BungeeSystem.getInstance().log(log);
    }

    public void removeAbuse(Abuse abuse) {
        abuses.remove(abuse);

        DatabaseManager.removeAbuse(abuse);

        String log = Constants.TAG;
        if (abuse.getAction() == AbuseAction.BAN) {
            log = "§e" + abuse.getAbuser() + " §7(" + abuse.getAbuserUuid().toString() + ") " + "wurde entbannt.";
        } else if (abuse.getAction() == AbuseAction.MUTE) {
            log = "§e" + abuse.getAbuser() + " §7(" + abuse.getAbuserUuid().toString() + ") " + "wurde entmutet.";
        }

        BungeeSystem.getInstance().log(log);
    }

    public void cache(UUID uuid, String name) {
        // Cache player
        playerCache.put(uuid, name);
    }

    public Abuse isBanned(UUID uuid) {
        for (Abuse abuse : abuses) {
            if (abuse.getAction() == AbuseAction.BAN && abuse.getAbuserUuid().equals(uuid)) {
                if (abuse.getLastTo() == -1) {
                    return abuse;
                } else {
                    if (abuse.getLastTo() < System.currentTimeMillis()) {
                        removeAbuse(abuse);
                        return null;
                    } else {
                        return abuse;
                    }
                }
            }
        }

        return null;
    }

    public Abuse isMuted(UUID uuid) {
        for (Abuse abuse : abuses) {
            if (abuse.getAction() == AbuseAction.MUTE && abuse.getAbuserUuid().equals(uuid)) {
                if (abuse.getLastTo() == -1) {
                    return abuse;
                } else {
                    if (abuse.getLastTo() < System.currentTimeMillis()) {
                        removeAbuse(abuse);
                        return null;
                    } else {
                        return abuse;
                    }
                }
            }
        }

        return null;
    }

    public Object[] getPlayerData(String name) {
        // Onnline
        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(name);
        if (player != null) {
            return new Object[] { player.getUniqueId(), player.getName() };
        }
        // Cache
        for (Map.Entry<UUID, String> entry : playerCache.entrySet()) {
            if (entry.getValue().equalsIgnoreCase(name)) {
                return new Object[] { entry.getKey(), entry.getValue() };
            }
        }
        // Mojang API
        try {
            String website = Util.getText("https://api.mojang.com/users/profiles/minecraft/" + name + "?unsigned=false");
            JsonObject object = Jsoner.deserialize(website, new JsonObject());

            StringBuilder str = new StringBuilder(object.getString("id"));

            str.insert(8, '-');
            str.insert(13, '-');
            str.insert(18, '-');
            str.insert(23, '-');

            UUID uuid = UUID.fromString(str.toString());
            String realName = object.getString("name");

            return new Object[] { uuid, realName };
        } catch (Exception e) {
            BungeeSystem.getInstance().log(Constants.TAG + "§cDer Name " + name + " konnte nicht auf mojang.com gefunden werden.");
            return new Object[0];
        }
    }

    // Clear old temp mute and bans
    private void clear() {
        for (Abuse abuse : abuses) {
            if (abuse.getLastTo() == -1) {
                continue;
            }

            long lastTo = abuse.getLastTo();
            if (lastTo < System.currentTimeMillis()) {
                abuses.remove(abuse);
            }
        }
    }
}
