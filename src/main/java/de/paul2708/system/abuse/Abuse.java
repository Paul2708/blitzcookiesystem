package de.paul2708.system.abuse;

import java.io.Serializable;
import java.util.UUID;

/**
 * Created by Paul on 11.09.2016.
 */
public class Abuse implements Serializable {

    private AbuseAction action;
    private String staff;
    private String abuser;
    private String reason;
    private long timeStamp;
    private long lastTo;
    private UUID staffUuid;
    private UUID abuserUuid;

    public Abuse action(AbuseAction action) {
        this.action = action;
        return this;
    }

    public Abuse staff(String staff) {
        this.staff = staff;
        return this;
    }

    public Abuse abuser(String abuser) {
        this.abuser = abuser;
        return this;
    }

    public Abuse reason(String reason) {
        this.reason = reason;
        return this;
    }

    public Abuse timeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public Abuse lastTo(long lastTo) {
        this.lastTo = lastTo;
        return this;
    }

    public Abuse staffUuid(UUID staffUuid) {
        this.staffUuid = staffUuid;
        return this;
    }


    public Abuse abuserUuid(UUID abuserUuid) {
        this.abuserUuid = abuserUuid;
        return this;
    }

    public AbuseAction getAction() {
        return action;
    }

    public String getStaff() {
        return staff;
    }

    public String getAbuser() {
        return abuser;
    }

    public String getReason() {
        return reason;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public long getLastTo() {
        return lastTo;
    }

    public UUID getStaffUuid() {
        return staffUuid;
    }

    public UUID getAbuserUuid() {
        return abuserUuid;
    }
}
