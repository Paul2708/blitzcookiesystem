package de.paul2708.system;

import de.paul2708.system.abuse.AbuseManager;
import de.paul2708.system.command.*;
import de.paul2708.system.file.ConfigFile;
import de.paul2708.system.listener.ChatListener;
import de.paul2708.system.listener.LoginListener;
import de.paul2708.system.mysql.DatabaseManager;
import de.paul2708.system.mysql.MySQL;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

/**
 * Created by Paul on 28.07.2017.
 */
public class BungeeSystem extends Plugin {

    private static BungeeSystem instance;

    private MySQL mySQL;

    private AbuseManager abuseManager;

    @Override
    public void onEnable() {
        BungeeSystem.instance = this;

        // File
        ConfigFile config = new ConfigFile(getDataFolder());
        config.load();

        // MySQL
        this.mySQL = new MySQL(config.getIp(), config.getDatabase(), config.getUser(), config.getPassword());
        mySQL.openConnection();

        DatabaseManager.setupTables();

        // Abuse manager
        this.abuseManager = new AbuseManager();
        abuseManager.resolveAbuses();

        // Commands
        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();
        pluginManager.registerCommand(this, new ReportCommand());
        pluginManager.registerCommand(this, new JumpToTargetCommand());
        pluginManager.registerCommand(this, new KickCommand());
        pluginManager.registerCommand(this, new BanCommand());
        pluginManager.registerCommand(this, new UnBanCommand());
        pluginManager.registerCommand(this, new TempBanCommand());
        pluginManager.registerCommand(this, new MuteCommand());
        pluginManager.registerCommand(this, new UnMuteCommand());
        pluginManager.registerCommand(this, new BungeeSystemCommand());

        // Listener
        pluginManager.registerListener(this, new LoginListener());
        pluginManager.registerListener(this, new ChatListener());
    }

    @Override
    public void onDisable() { }

    public static BungeeSystem getInstance() {
        return instance;
    }

    public void log(String message) {
        // Log message in console
        ProxyServer.getInstance().getConsole().sendMessage(new TextComponent(message));
    }

    public MySQL getMySQL() {
        return mySQL;
    }

    public AbuseManager getAbuseManager() {
        return abuseManager;
    }
}
