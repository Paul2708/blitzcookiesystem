package de.paul2708.system.mysql;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.abuse.Abuse;
import de.paul2708.system.util.Constants;
import net.md_5.bungee.api.ProxyServer;

import java.sql.PreparedStatement;

/**
 * Created by Paul on 07.08.2016.
 */
public class DatabaseManager {

    // Tables
    public static void setupTables() {
        String query = "CREATE TABLE IF NOT EXISTS `abuses` ("
                + "`id` integer(200) NOT NULL AUTO_INCREMENT,"
                + "`action` varchar(200) NOT NULL,"
                + "`staff` varchar(200) NOT NULL,"
                + "`abuser` varchar(200) NOT NULL,"
                + "`reason` varchar(200) NOT NULL,"
                + "`timestamp` varchar(200) NOT NULL,"
                + "`last_to` varchar(200) NOT NULL,"
                + "`staff_uuid` varchar(200) NOT NULL,"
                + "`abuser_uuid` varchar(200) NOT NULL,"
                + "PRIMARY KEY (`id`)) "
                + "ENGINE=InnoDB DEFAULT CHARSET=latin1;";
        BungeeSystem.getInstance().getMySQL().queryUpdate(query);
    }

    // Add abuse to table
    public static void addAbuse(Abuse abuse) {
        ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), () -> {
            try {
                PreparedStatement update = BungeeSystem.getInstance().getMySQL().getConnection().prepareStatement(
                        "INSERT IGNORE INTO `abuses` ("
                                + "`action`, `staff`, `abuser`, `reason`, `timestamp`, `last_to`, `staff_uuid`, `abuser_uuid`"
                                + ") VALUES ("
                                + "?, ?, ?, ?, ?, ?, ?, ?"
                                + ")");
                update.setString(1, abuse.getAction().getData());
                update.setString(2, abuse.getStaff());
                update.setString(3, abuse.getAbuser());
                update.setString(4, abuse.getReason());
                update.setString(5, abuse.getTimeStamp() + "");
                update.setString(6, abuse.getLastTo() + "");
                update.setString(7, abuse.getStaffUuid().toString());
                update.setString(8, abuse.getAbuserUuid().toString());

                update.executeUpdate();
            } catch (Exception e) {
                BungeeSystem.getInstance().log(Constants.TAG + "§cDer Abuse konnte nicht gepeichert werden.");
                e.printStackTrace();
            }
        });
    }

    public static void removeAbuse(Abuse abuse) {
        ProxyServer.getInstance().getScheduler().runAsync(BungeeSystem.getInstance(), () -> {
            try {
                PreparedStatement update = BungeeSystem.getInstance().getMySQL().getConnection().prepareStatement(
                        "DELETE FROM `abuses` WHERE `action` = ? AND `abuser_uuid` = ?");
                update.setString(1, abuse.getAction().getData());
                update.setString(2, abuse.getAbuserUuid().toString());

                update.executeUpdate();
            } catch (Exception e) {
                BungeeSystem.getInstance().log(Constants.TAG + "§cDer Abuse konnte nicht gelöscht werden.");
                e.printStackTrace();
            }
        });
    }

}
