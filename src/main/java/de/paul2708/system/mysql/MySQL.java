package de.paul2708.system.mysql;

import de.paul2708.system.BungeeSystem;
import de.paul2708.system.util.Constants;

import java.sql.*;

/**
 * Created by Paul on 07.08.2016.
 */
public class MySQL {

    private Connection connection;
    private String host = "localhost", database, user, password;
    private int port = 3306;

    public MySQL(String host, String database, String user, String password) {
        this.host = host;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public Connection openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.connection = DriverManager.getConnection("jdbc:mysql://" + this.host + ":" + this.port + "/" + this.database + "?user=" + this.user + "&password=" + this.password + "&autoReconnect=true");
            BungeeSystem.getInstance().log(Constants.TAG + "§aVerbindung zur Datenbank '" + this.database + "' wurde hergestellt.");
        } catch (SQLException e) {
            BungeeSystem.getInstance().log(Constants.TAG + "§cVerbindung zur Datenbank '" + this.database + "' konnte nicht hergestellt werden.");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            BungeeSystem.getInstance().log(Constants.TAG + "§cMySQL-Treiber wurde nicht gefunden.");
        }

        return this.connection;
    }

    public boolean hasConnection() {
        try {
            return (this.connection != null) || (this.connection.isValid(1));
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void queryUpdate(String query) {
        Connection con = MySQL.this.connection;
        PreparedStatement st = null;
        try {
            st = con.prepareStatement(query);
            st.executeUpdate();
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            MySQL.this.closeResources(null, st);
        }
    }

    public ResultSet getQuery(String query) {
        try {
            PreparedStatement stmt = this.connection.prepareStatement(query);
            return stmt.executeQuery();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void closeResources(ResultSet rs, PreparedStatement st) {
        try {
            if(rs != null) rs.close();
            if(st != null) st.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void closeConnection() {
        try {
            this.connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            this.connection = null;
        }
    }

    public Connection getConnection() {
        return connection;
    }
}
