package de.paul2708.system.util;

/**
 * Created by Paul on 28.07.2017.
 */
public class Constants {

    public static final String TAG = "§8[§3System§8] §r";

    public static final String BYPASS_PERMISSION = "netzwerk.bypass";
    public static final String GET_REPORTS_PERMISSION = "netzwerk.anzeige";
    public static final String KICK_PERMISSION = "netzwerk.kick";
    public static final String BAN_PERMISSION = "netzwerk.ban";
    public static final String MUTE_PERMISSION = "netzwerk.mute";
    public static final String UNMUTE_PERMISSION = "netzwerk.unmute";
    public static final String BUNGEESYSTEM_PERMISSION = "netzwerk.bungeesystem";
}
