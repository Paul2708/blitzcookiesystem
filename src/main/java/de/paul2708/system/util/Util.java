package de.paul2708.system.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Paul on 28.07.2017.
 */
public class Util {

    // by Joseph Weissman
    public static String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null)
            response.append(inputLine);

        in.close();

        return response.toString();
    }

    // Time
    private static DateFormat format;

    static {
        format = new SimpleDateFormat("dd.MM.yyyy - HH:mm:ss");
    }

    public static String getTime(long time) {
        return format.format(new Date(time));
    }
}
